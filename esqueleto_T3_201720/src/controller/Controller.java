package controller;

import java.io.File;

import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import model.vo.StopVO;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops();		
	}

	public static void readBusUpdates() {
		File f = new File("data");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length-1; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	
	public static IStack<StopVO> listStops(Integer tripId) throws TripNotFoundException{
		return manager.listStops(tripId);
	}
	

}
