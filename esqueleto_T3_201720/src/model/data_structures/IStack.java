package model.data_structures;

import model.data_structures.LinkedStack.Node;

public interface IStack<E> {

	public void push (E item);
	
	public E pop();
	
	public int getSize();
	
	public Node<E> getFirst();
}
