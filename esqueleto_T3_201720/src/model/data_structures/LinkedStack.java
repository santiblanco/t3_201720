package model.data_structures;

import java.util.NoSuchElementException;


public class LinkedStack<E> implements IStack<E> {

	//Atributos

	/**
	 * Primer nodo de la pila
	 */
	private Node<E> first;

	/**
	 * Tama�o de la pila
	 */

	private int n;            	

	/**
	 * Clase de los nodos
	 */

	public static class Node<E> {
		public E item;
		public Node<E> next;
	}

	public boolean isEmpty(){
		return first == null;
	}

	public int getSize(){
		return n;
	}

	public void push(E item) {
		Node<E> oldFirst = first;
		first = new Node<E>();
		first.item = item;
		first.next = oldFirst;
		n++;
	}

	public E pop() {
		E item = first.item;
		first = first.next;
		return item;
	}

	public E peek(){
		if (isEmpty()) throw new NoSuchElementException("LinkedStack underflow");
		return first.item;
	}
	
	public Node<E> getFirst(){
		if (isEmpty()) throw new NoSuchElementException("LinkedStack underflow");
		return first;
	}






}
