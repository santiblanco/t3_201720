package model.data_structures;

import java.util.NoSuchElementException;

public class Queue<E> implements IQueue<E> {
	
	
	private Node<E> first;    // beginning of queue
    private Node<E> last;     // end of queue
    private int n;               // number of elements on queue

    // helper linked list class
    public static class Node<E> {
        public E item;
        public Node<E> next;
    }

    /**
     * Initializes an empty queue.
     */
    public Queue() {
        first = null;
        last  = null;
        n = 0;
    }

    /**
     * Retorna verdadero si la cola est� vac�a
     *
     * @return true si la cola esta vacia. Falso en lo contrario
     */
    public boolean isEmpty() {
        return first == null;
    }
    
    /**
     * Returns the number of items in this queue.
     *
     * @return the number of items in this queue
     */
    public int size() {
        return n;
    }


	@Override
	public void enqueue(E item) {
		// TODO Auto-generated method stub
        Node<E> oldlast = last;
        last = new Node<E>();
        last.item = item;
        last.next = null;
        if (isEmpty()){
        	first = last;
        }
        else{           
        	oldlast.next = last;
        }
        n++;
    }

	@Override
	public E dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        E item = first.item;
        first = first.next;
        n--;
        if (isEmpty()) last = null;   // to avoid loitering
        return item;
    }
	
	public Node<E> getLast(){
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
		return last;
	}
	/**
	 * Devuelve el primer elemento de la cola
	 * @return first.item
	 */
	
	 public E peek() {
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        return first.item;
	    }
	 public Node<E> getFirst(){
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        return first;
		 
	 }

}
