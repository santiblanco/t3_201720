package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ISTSManager;
import model.data_structures.IStack;
import model.data_structures.LinkedStack;
import model.data_structures.LinkedStack.Node;
import model.data_structures.Queue;
import model.vo.BusUpdateVO;
import model.vo.StopVO;

public class STSManager implements ISTSManager{

	LinkedStack<BusUpdateVO> busList = new LinkedStack<>();
	Queue<StopVO> stopList = new Queue<>();

	@Override
	public void readBusUpdate(File rtFile) {

		// TODO Auto-generated method stub
		try {
			Gson gson = new GsonBuilder().create();
			InputStream stream = new FileInputStream(rtFile);
			JsonReader reader = new JsonReader(new InputStreamReader(stream,"UTF-8"));
			reader.beginArray();
			while(reader.hasNext()) {
				BusUpdateVO bus = gson.fromJson(reader, BusUpdateVO.class);
				busList.push(bus);
			}

			reader.close();
		}
		catch(Exception e) {
			System.out.println("Error en loadBusUpdate()\n");
			System.out.println(e.getMessage()+"------------------------------------\n");
			System.out.println("------------------------------------\n");
			System.out.println("------------------------------------\n");
			System.out.println("------------------------------------\n");

		}
	}
	public BusUpdateVO searchTrip(Integer id) {
		BusUpdateVO ans = null;
		boolean found = false;
		Node<BusUpdateVO> current = busList.getFirst();
		while(current.next!= null && !found) {
			if(current.item.getTripId()==id) {
				ans = current.item;
				found = true;
			}
			current = current.next;
		}
		return ans;
	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		IStack<StopVO> ans = new LinkedStack<>();
		Node<BusUpdateVO> trip = busList.getFirst();
		while(trip.next!= null) {
			if(trip.item.getTripId()==tripID) {
				model.data_structures.Queue.Node<StopVO> current = stopList.getFirst();
				while(current.next!=null) {
					if(getDistance(trip.item.getLatitude(), trip.item.getLongitude(), current.item.getStopLat(), current.item.getStopLong())<=70) {
						ans.push(current.item);
					}
					current = current.next;
				}
			}
			trip = trip.next;
		}

		return ans;
	}

	@Override
	public void loadStops() {
		// TODO Auto-generated method stub
		{
			try {
				BufferedReader reader = new BufferedReader(new FileReader("data/stops.txt"));
				String line = reader.readLine();
				line = reader.readLine();

				while(line != null){
					String[] info = line.split(",");
					String stop_id = info[0].trim();
					String stop_code = info[1].trim();
					String stop_name = info[2].trim();
					String stop_desc = info[3].trim();
					String stop_lat = info[4].trim();
					String stop_lon = info[5].trim();
					String zone_id = info[6].trim();
					String stop_url = info[7].trim();
					String location_type = info[8].trim();
					String parent_station = (info.length==9)?"":info[9];

					int intStopId = (stop_id.equals(""))?0:Integer.parseInt(stop_id);
					int intStopCode = (stop_code.equals(""))?0:Integer.parseInt(stop_code);
					double doubleStopLat = (stop_lat.equals(""))?0:Double.parseDouble(stop_lat);
					double doubleStopLon = (stop_lon.equals(""))?0:Double.parseDouble(stop_lon);
					int intLocationType = (location_type.equals(""))?0:Integer.parseInt(location_type);

					StopVO stop = new StopVO(intStopId,intStopCode,stop_name,stop_desc,doubleStopLat,doubleStopLon,zone_id,stop_url,intLocationType,parent_station);
					stopList.enqueue(stop);
					line = reader.readLine();
				}
				reader.close();
			}
			catch(Exception e) {
				System.out.println("Error en loadStops()\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");

			}

		}
	}
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}


}
