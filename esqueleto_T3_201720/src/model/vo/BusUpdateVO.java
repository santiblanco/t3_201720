package model.vo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BusUpdateVO {

	private String VehicleNo;
	
	private  int TripId;
	
	private String RouteNo;
	
	private String Direction;
	
	private String Destination;
	
	private String Pattern;
	
	private double Latitude;
	
	private double Longitude;
	
	private String RecordedTime;
	
	private class RouteMap{
		public String Href;
	}
	
	private RouteMap RouteMap;
	
	
	public BusUpdateVO() {
		VehicleNo = null;
		TripId = 0;
		RouteMap = null;
		RouteNo = null;
		Direction = null;
		Destination= null;
		Pattern = null;
		Latitude = 0;
		Longitude = 0;
		RecordedTime = null;
	}

	public double getLatitude() {
		return Latitude;
	}

	public void setLatitude(double latitude) {
		this.Latitude = latitude;
	}

	public String getVehicleNo() {
		return VehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.VehicleNo = vehicleNo;
	}

	public int getTripId() {
		return TripId;
	}

	public void setTripId(int tripId) {
		this.TripId = tripId;
	}

	public String getRouteNo() {
		return RouteNo;
	}

	public void setRouteNo(String routeNo) {
		this.RouteNo = routeNo;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		this.Direction = direction;
	}

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		this.Destination = destination;
	}

	public double getLongitude() {
		return Longitude;
	}

	public void setLongitude(double longitude) {
		this.Longitude = longitude;
	}

	public String getPattern() {
		return Pattern;
	}

	public void setPattern(String pattern) {
		this.Pattern = pattern;
	}

	public Calendar getRecordedTime() {
		Calendar cal = Calendar.getInstance();
		String format = "hh/mm/ss am/pm";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			cal.setTime(sdf.parse(RecordedTime));
		}
		catch(Exception e) {
			System.out.println("Error en BusUpdateVO.getRecordedTime");
		}
		
		return cal;
	}

//	public void setRecordedTime(Calendar recordedTime) {
//		this.RecordedTime = recordedTime;
//	}


	
	public String toString() {
		return "{VehicleNo:"+VehicleNo+",TripId:"+TripId+",RouteNo:"+RouteNo+",Direction:"+Direction+",Destination:"+Destination+",Pattern:"+Pattern
				+",Latitude:"+Latitude+",Longitude:"+Longitude+",RecordedTime:"+RecordedTime+",RouteMap"+":{Href:"+RouteMap.Href+"}"+RouteMap+'}';
	}

}
