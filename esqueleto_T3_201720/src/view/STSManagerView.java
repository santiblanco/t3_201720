package view;

import java.util.Scanner;

import model.data_structures.IStack;
import model.data_structures.LinkedStack.Node;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.loadStops();
					break;
				case 2:
					long memoryBefore = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					Controller.readBusUpdates();
					long memoryAfter = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					Runtime.getRuntime().gc();
					System.out.println("Se estan utilizando aproximadamente " + ((memoryAfter - memoryBefore)/1000000.0) + " MB");
					break;
				case 3:
					System.out.println("Ingrese el id del viaje:");
					Integer tripId = sc.nextInt();
				try {
					IStack<StopVO> list = Controller.listStops(tripId);
					Node<StopVO> current = list.getFirst();
					while(current.next!=null) {
						System.out.println(current.item.getName());
						current = current.next;
					}
					
				} catch (TripNotFoundException e) {
					System.out.println("El id ingresado no existe");
				}
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de paradas (data/stops.txt)");
		System.out.println("2. Cargar actualizaciones de buses");
		System.out.println("3. Dar paradas de un viaje");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}
}
