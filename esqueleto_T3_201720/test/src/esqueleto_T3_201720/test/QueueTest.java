package esqueleto_T3_201720.test;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.Queue.Node;

public class QueueTest extends TestCase {
	
	//ATTRIBUTES

		Queue<Integer> queueTest;;
		Node<Integer> first;
		Node<Integer> last;

		private void setupEscenario1(){
			try{
				queueTest = new Queue<Integer>();
			}
			catch(Exception e){
				fail("No debi� generar error al crear la lista");
			}
		}
		
		private void setupEscenario2(){
			try{
			
				queueTest = new Queue<Integer>();
				queueTest.enqueue(1);
				queueTest.enqueue(2);
				queueTest.enqueue(3);

			}
			catch(Exception e){
				fail("No debi� generar error al crear la lista");
			}
		}
		
		public void testQueue(){
			setupEscenario1();
			assertEquals("Al inicio, no debe haber ning�n elemento",0,queueTest.size());
		}
		
		public void testGetSize(){
			setupEscenario2();
			assertEquals("Debe tener tres nodos",3,queueTest.size());
		}
		
		public void testDequeue(){
			setupEscenario2();
			assertEquals("El dequeue debe retornar el primer elemento",new Integer(1),queueTest.dequeue());
		}
		
		public void testEnqueue(){
			setupEscenario1();
			queueTest.enqueue(1);
			assertEquals("La lista debe tener un elemento despues del enqueue",1,queueTest.size());
		}
		
		public void testPeek(){
			setupEscenario2();
			assertEquals("El primer elemento debe ser 1",new Integer(3),queueTest.peek());
		}
		
	
	

}
