package esqueleto_T3_201720.test;

import junit.framework.TestCase;
import model.data_structures.LinkedStack;
import model.data_structures.LinkedStack.Node;

public class StackTest extends TestCase {

	//ATTRIBUTES

	LinkedStack<Integer> stackTest;;
	Node<Integer> first;
	Node<Integer> last;

	private void setupEscenario1(){
		try{
			stackTest = new LinkedStack<Integer>();
		}
		catch(Exception e){
			fail("No debi� generar error al crear la lista");
		}
	}
	
	private void setupEscenario2(){
		try{
		
			stackTest = new LinkedStack<Integer>();
			stackTest.push(1);
			stackTest.push(2);
			stackTest.push(3);

		}
		catch(Exception e){
			fail("No debi� generar error al crear la lista");
		}
	}
	
	public void testStackList(){
		setupEscenario1();
		assertEquals("Al inicio, no debe haber ning�n elemento",0,stackTest.getSize());
	}
	
	public void testGetSize(){
		setupEscenario2();
		assertEquals("Debe tener tres nodos",3,stackTest.getSize());
	}
	
	public void testPop(){
		setupEscenario2();
		assertEquals("El pop debe retornar 3",new Integer(3),stackTest.pop());
	}
	
	public void testPush(){
		setupEscenario1();
		stackTest.push(1);
		assertEquals("La lista debe tener un elemento",1,stackTest.getSize());
	}
	
	public void testPeek(){
		setupEscenario2();
		assertEquals("El primer elemento debe ser 3",new Integer(3),stackTest.peek());
	}


}
